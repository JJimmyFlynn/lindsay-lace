<?php @header("HTTP/1.1 404 Not found", TRUE, 404); ?>
<?php get_header() ?>
<div class="grid">
	<section role="main" class="content col col-2-3">
		<h1>Sorry, the page you're looking for cannot be found</h1>
	</section>
</div>

<?php get_footer(); ?>