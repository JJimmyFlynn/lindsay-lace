<?php
/*
Template Name: About
*/
?>

<?php get_header(); ?>

<div class="grid">
	<section role="main" class="content col col-2-3">
		<div class="flexslider aboutSlider">
			<ul class="slides">
		<?php 

		$query = new WP_Query(array(
			'post_type' => 'about_images'
		)); ?>
		<?php if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
		    <li data-thumb="<?php 
		    	$thumb_id = get_post_thumbnail_id();
		    	$thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail_200_200', true);
		    	echo $thumb_url[0];
		    ?>">
		      <?php the_post_thumbnail(); ?>
		    </li>
  <?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>
		</ul>
		</div>

		<div class="aboutText">
			<?php the_content(); ?>
		</div>
		<img class="aboutLogo" src="http://www.lindsayandlace.com/wp-content/themes/Lindsay%26Lace/assets/img/logoLG.png" alt="Lindsay &amp; Lace">
	</section>
<?php include('sidebar.php'); ?>
</div> <!-- /.grid -->


<?php get_footer(); ?>