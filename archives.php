<?php
/*
Template Name: Archives
*/
?>
<?php get_header(); ?>

<div class="grid">
	<section role="main" class="content col col-2-3">
	<h1>Archives</h1>
	<!-- 2014 Posts -->
	<?php query_posts( 'year=2014&posts_per_page=-1' ); ?>
	<?php if (have_posts()) : ?> 
		<h2>2014</h2>
		<div class="archiveGroup">
			<?php while (have_posts()) : the_post(); ?>
			<?php if(has_post_thumbnail()) { ?>
			<div class="archiveListing">
				<a href="<?php the_permalink(); ?>">
					<?php the_post_thumbnail( $size = 'thumbnail_200_200' ); ?>
				</a>
			<?php } ?>
		</div>

	<?php endwhile; endif; ?>
	</div>
	<?php wp_reset_query(); ?>

<!-- 2013 Posts -->
	<?php query_posts( 'year=2013&posts_per_page=-1' ); ?>
	<?php if (have_posts()) : ?> 
		<h2>2013</h2>
		<div class="archiveGroup">
	<?php while (have_posts()) : the_post(); ?>
	<?php if(has_post_thumbnail()) { ?>
	<div class="archiveListing">
		<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail( $size = 'thumbnail_200_200' ); ?>
		</a>
	<?php } ?>
	</div>

	<?php endwhile; endif; ?>
	</div>
	<?php wp_reset_query(); ?>
	</section>
	<?php include('sidebar.php'); ?>
</div> <!-- /.grid -->


<?php get_footer(); ?>