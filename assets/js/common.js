getInstagram = function() {
	//Variables for Instagram Ajax Call
	var dataUrl = 'https://api.instagram.com/v1/users/22898338/media/recent';
	var accessParameters = 'client_id=bccac5cb67be4efeb5ae0b2cca77b37b&count=4';

	//Call for Latest Instagram Photos
	$.ajax(dataUrl, {
		data: accessParameters,
		dataType: 'JSONP',
		success: function(data) {
			for (var i = 0; i < data.data.length; i++) {
				$('.instagram').append("<a href='" + data.data[i].link + "'" + " target='_blank'>" + "<img src='" +
				data.data[i].images.thumbnail["url"] +
				"'>" + "</a>");
			}
		}
	});
};

//Get Instagram photos for sidebar
enquire.register("screen and (min-width:860px)", {
	match: function() {
		if($('.instagram img').length === 0) {
			getInstagram();
		}
	}
	});

//Side Nav Toggle
$(".menuToggle, .slideNavOverlay").on('click', function() {
	$('body').toggleClass("slideNavActive");
	$(".menuToggle i").toggleClass("icon-menu").toggleClass("icon-close");
});

enquire.register("screen and (max-width:860px)", {
	unmatch: function() {
		$('body').removeClass('slideNavActive');
		if($('.menuToggle i').hasClass('icon-close')) {
			$('.menuToggle i').removeClass('icon-close').addClass('icon-menu');
		}
	}
});

//----------Recent Posts Carousel
$('.recentPosts').owlCarousel({
	items: 5,
	navigation: true,
	pagination: false,
	navigationText: ["", ""],
	itemsDesktop: [1199,5],
	itemsDesktopSmall: [979,5],
	itemsTablet: [768,4],
	itemsMobile: [479, 1]
});

//----------About Page Slider

$('.aboutSlider').flexslider({
	controlNav: 'thumbnails',
	directionNav: false,
	slideshow: false
});

//Fixed Header
$(".siteHeader").headroom({
	offset : 24,
	classes: {
		initial: "fixedHeader",
		pinned: "slideIn",
		unpinned: "slideOut",
	}
});


//----------Floating Sidebar
enquire.register("screen and (min-width:860px)", {
	match: function() {
								var $sidebar   = $("#sidebar"),
        $window    = $(window),
        topPadding,
        offset;

        if($('body').hasClass('home')){
         offset     = 529;
         topPadding = 40;
        }
        else {
         offset = 247;
         topPadding = 10;
        }
		if($(window).height() > 720) {

    $window.scroll(function() {
        if ($window.scrollTop() > offset) {
            $sidebar.stop().animate({
                marginTop: ($window.scrollTop() - offset + topPadding)
            });
        } else {
            $sidebar.stop().animate({
                marginTop: 0
            });
        }
    });
		}
	}
});