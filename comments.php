<div class="commentsArea">
	<?php comment_form(array(
		'comment_notes_after' => ''
	)); ?>

	<div class="comments">
		<?php wp_list_comments(array(
			'type' => 'comment'
		)); ?>
	</div> <!-- /.comments -->
</div> <!-- /.commentsArea -->