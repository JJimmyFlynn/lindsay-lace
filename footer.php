	</div> <?php #opened in header.php ?>
	<div class="slideNavOverlay"></div>
	<footer role="contentinfo" class="siteFooter">
		<div class="container">
			<p>All Rights Reserved &copy; Lindsay and Lace <?php the_time('Y');?></p>
			<div class="footerSocial">
				<?php include('parts/social-links.php'); ?>
		</div> <!-- /.headerSocial -->
		</div>
	</footer>
</div> <?php #opened in header.php ?>

<!--WP Generated Footer -->
<?php wp_footer(); ?>
<!--End WP Generated Footer -->

<!-- Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-40236598-1', 'lindsayandlace.com');
  ga('send', 'pageview');

</script>

</body>

</html> 