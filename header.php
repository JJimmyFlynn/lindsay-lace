<!DOCTYPE html>
<html lang="en">

<head>

		<meta charset="utf-8" />
		<title><?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">

		<!-- stylesheets -->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/assets/css/global.css"/>

		<script type="text/javascript" src="//use.typekit.net/wwr0pqb.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>

		<!--WP Generated Header -->
		<?php wp_head(); ?>
		<!--End WP Generated Header -->
		
</head>
<body <?php body_class($class); ?>>
		<?php include('parts/mobile-nav.php'); ?>
		<div class="siteWrap"> <?php #closed in footer.php ?>
		<header class="siteHeader" role="banner">
				<div class="container">
		    <a href="#0" class="menuToggle"><i class="icon-menu"></i></a>
		    <div class="logoBox">
		    	<a href="/">
				    <img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logoSM.png" alt="Lindsay &amp; Lace" class="logoSM">
			   	</a>
			   </div>
						<nav class="mainNav" role="navigation">
							<?php include('parts/menu.php'); ?>
						</nav>

						<div class="headerSocial">
							<?php include('parts/social-links.php'); ?>
						</div> <!-- /.headerSocial -->

				</div> <!-- /.container -->
		</header>

		<div class="container"> <?php #closed in footer.php ?>

				<div class="siteLogo">
						<h1>
								<a href="/">
										<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logoLG.png" alt="Lindsay &amp; Lace">
								</a>
						</h1>
				</div>