<?php get_header(); ?>
<div class="grid">
	<section role="main" class="content col col-2-3">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="blogPost">
			<?php the_content(); ?>
		</article>
		<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>
	</section>
	<?php include('sidebar.php'); ?>
</div> <!-- /.grid -->

<?php get_footer(); ?>