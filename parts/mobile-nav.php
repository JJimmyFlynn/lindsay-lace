<div class="slideNav">
	<div class="logo">
		<a href="/">
			<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logoLG.png" alt="Lindsay &amp; Lace">
		</a>
	</div>
  <nav role="navigation" class="mobileNav">
    <?php include('menu.php'); ?>
  </nav>
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<input type="search" class="search-field" placeholder="Search" value="" name="s" title="Search for:" />
		</form>
		<div class="slideSocial">
			<?php include('social-links.php'); ?>
		</div>
		<div class="slideContact">
			<?php the_field('mobile_menu_blurb', 'option'); ?>
			<a class="btn" href="mailto:lindsay@lindsayandlace.com">Email me</a>
		</div>
</div>