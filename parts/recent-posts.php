<div class="recentPosts owl-carousel">
  <?php query_posts( 'posts_per_page=8' ); ?>
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div>
    	<a href="<?php the_permalink(); ?>">
      <?php the_post_thumbnail( $size = 'thumbnail_200_150') ?>
      <p class="flex-caption"><?php the_title(); ?></p>
     </a>
    </div>
  <?php endwhile; endif; ?>
  <?php wp_reset_query(); ?>
</div>