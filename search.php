<?php get_header(); ?>
<div class="grid">
	<section role="main" class="content col col-2-3">
		<h2>Search Results</h2>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="searchResult">
			<div class="resultImage col col-1-4">
				<?php the_post_thumbnail( $size = 'thumbnail_200_200') ?>
				<!-- <img src="http://placehold.it/200x200" alt=""> -->
			</div> <!-- /.resultImage -->
			<div class="resultExcerpt col col-3-4">
				<h3 class="h2"><?php the_title(); ?></h3>
				<?php the_excerpt(); ?>
				<a href="<?php the_permalink(); ?>" class="details">Details <span class="icon-arrow-right"></span></a>
			</div> <!-- /.resultExcerpt -->

		</div>

<?php endwhile; else: ?>
	<p>Sorry, no results for '<?php the_search_query(); ?>'</p>
<?php endif; ?>
<?php wp_reset_query(); ?>
	</section>
	<?php include('sidebar.php'); ?>
</div> <!-- /.grid -->

<?php get_footer(); ?>