<?php
/*
Template Name: Sidebar
*/
?>
<aside role="complementary" class='sidebar col col-1-4' id='sidebar'>
	<div class="instagram">
		<h3 class="h2">Instagram</h3>

	</div> <!-- /.instagram -->

	<div class="contact">
		<h3 class="h2">Contact</h3>
		<?php the_field('sidebar_blurb', 'option'); ?>
		<p class="emailMe">EMAIL ME: Lindsay@lindsayandlace.com</p>
	</div> <!-- /.contat -->

	<div class="search">
		<h3 class="h2">Search</h3>
		<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
			<input type="search" class="search-field" placeholder="Search" value="" name="s" title="Search for:" />
			<input type="submit" class="search-submit btn" value=" " />
		</form>
	</div>
</aside>