<?php get_header(); ?>

<div class="grid">
	<section role="main" class="content col col-2-3">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<article class="blogPost">
			<time class="postDate"><?php the_time('F j, Y'); ?></time>
			<h2 class="postTitle">
			<?php the_title(); ?>
			</h2>
			<div class="postContent">
				<?php if (has_post_thumbnail()) :
				the_post_thumbnail(); endif; ?>
				<div class="clothingListing">
					<?php if( have_rows('post_clothing_item') ):
						while ( have_rows('post_clothing_item') ) : the_row(); ?>
						<span class="clothingItem">
							<b><?php the_sub_field('item_type'); ?>: </b>
							<?php if(get_sub_field('item_link')) : ?>
							<a href="<?php the_sub_field('item_link'); ?>">
								<?php the_sub_field('item_name'); ?>
							</a>
							<?php else : ?>
							<?php the_sub_field('item_name'); ?>
							<? endif; ?>
						</span>
					<?php endwhile; endif; ?>
				</div>
				<?php the_content(); ?>
			<div class="postTag">
				<img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/postTag.png" alt="">
			</div>
			</div> <!-- /.postContent -->
			<div class="postSocial">
				<a href="#"><i class="icon-facebook"></i></a>
				<a href="#"><i class="icon-pinterest"></i></a>
				<a href="#"><i class="icon-twitter"></i></a>
				<a href="#"><i class="icon-googleplus"></i></a>
			</div> <!-- /.postSocial -->
			<?php related_posts(); ?>
			<div class="postNavigation">
				<span class="nextPost"><?php next_post_link('%link', 'NEXT') ?></span>
				<span class="prevPost"><?php previous_post_link('%link', 'PREV') ?></span>
			</div>
		</article>

		<?php comments_template( $file = '/comments.php', $separate_comments = false ); ?>
				<?php endwhile; endif; ?>
		<?php wp_reset_query(); ?>
	</section>
	<?php include('sidebar.php'); ?>
</div> <!-- /.grid -->


<?php get_footer(); ?>
