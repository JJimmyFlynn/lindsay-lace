<?php
/*
Template Name: Travel
*/
?>

<?php get_header(); ?>

<div class="grid">

	<section role="main" class="content category col col-2-3">
		<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
	<?php query_posts('category_name=travel&showposts=3&paged=' . $paged); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<article class="homePost blogPost">
			<div>
				<time class="postDate"><?php the_time('F j, Y'); ?></time>
			</div>
			<a href="<?php the_permalink(); ?>" class="feature">
				<div class="featuredImage">
					<? if (has_post_thumbnail()) {
						the_post_thumbnail();
					 } else { ?>
						<img src="<?php echo catch_that_image(); ?>" alt="">
					<?php } ?>
					<h2 class="postTitle">
						<?php the_title(); ?>
					</h2>
					<p class="details">Details <i class="icon-arrow-right"></i></p>
				</div> <!-- /.featuredImage -->
			</a>
			<div class="postContent">
				<?php if( have_rows('post_clothing_item') ):
				while ( have_rows('post_clothing_item') ) : the_row(); ?>
					<span class="clothingItem">
						<b><?php the_sub_field('item_type'); ?>: </b>
						<?php if(get_sub_field('item_link')) : ?>
						<a href="<?php the_sub_field('item_link'); ?>">
							<?php the_sub_field('item_name'); ?>
						</a>
						<?php else : ?>
						<?php the_sub_field('item_name'); ?>
						<? endif; ?>
					</span>
				<?php endwhile; endif; ?>
			</div> <!-- /.postContent -->
			<div class="postSocial">
				<a href="#"><i class="icon-pinterest"></i></a>
				<a href="#"><i class="icon-facebook"></i></a>
				<a href="#"><i class="icon-twitter"></i></a>
				<a href="#"><i class="icon-googleplus"></i></a>
			</div> <!-- /.postSocial -->
		</article>
		<?php endwhile; endif; ?>
		<div class="postNavigation">
			<span class="nextPost"><?php next_posts_link('NEXT') ?></span>
			<span class="prevPost"><?php previous_posts_link('PREV') ?></span>
		</div>
		<?php wp_reset_query(); ?>
	</section>
	<?php include('sidebar.php'); ?>
</div> <!-- /.grid -->


<?php get_footer(); ?>