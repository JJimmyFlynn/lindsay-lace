<?php 
/*
YARPP Template: L&L Related Posts
Author: John Flynn
Description: Related Posts Template for Lindsay & Lace Blog
*/
?><h3>Related Posts</h3>
<?php if (have_posts()):?>
	<?php while (have_posts()) : the_post(); ?>
	<div class="relatedPost">
		<a href="<?php the_permalink() ?>" rel="bookmark">
			<?php the_post_thumbnail( $size = 'thumbnail_200_150'); ?>
			<?php the_title(); ?>
		</a>
		<!-- (<?php the_score(); ?>)-->
	</div>
	<?php endwhile; ?>
<?php else: ?>
<p>No related posts.</p>
<?php endif; ?>
